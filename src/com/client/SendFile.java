package com.client;

import com.Config;

import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.util.*;

public class SendFile {
    private final Socket socket;
    private final Path filePath;

    public SendFile(Path filePath, Socket socket) {
        this.socket = socket;
        this.filePath = filePath;
    }


    public int send() throws InterruptedException {
        try {
            DataInputStream echoes = new DataInputStream(socket.getInputStream());

            //each time, read bytes from fileInputStream and write in to a byte array;
            int sizeEach = Config.SIZE_EACH;

            File file = new File(filePath.toString());
            FileInputStream fileInputStream = new FileInputStream(file.getAbsolutePath());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());


            //length of file
            long fileLength = file.length();

            long sendFullSizeTimes = fileLength / sizeEach; //number of times read full sizeEach bytes
            System.out.println("fullSizeEachTimes = " + sendFullSizeTimes);
            System.out.println("Reading bytes (may take a long time)...");
            int remainder = (int)fileLength % sizeEach; // số dư


            Queue<byte[]> queue = new LinkedList<>();


            int read = (int)sendFullSizeTimes;
            byte[] fileContentBytes;

            //read full sizeEach bytes
            while (read > 0){
                fileContentBytes = new byte[sizeEach];
                fileInputStream.read(fileContentBytes, 0, fileContentBytes.length);
                queue.add(fileContentBytes);
                read--;
            }

            //remainder at end of file
            if(remainder != 0){
                fileContentBytes = new byte[remainder];
                fileInputStream.read(fileContentBytes, 0, fileContentBytes.length);
                queue.add(fileContentBytes);
            }


            int fullQueueSize = queue.size();

            System.out.println("queue size: " + fullQueueSize);



            //send path, queue size and remainder first
            dataOutputStream.writeUTF(filePath.getFileName().toString());
            dataOutputStream.writeInt(fullQueueSize);
            dataOutputStream.writeInt(remainder);


            //number of times server writes part successful
            int partWillBeSent = 1;

            //stop when send all parts of the queue
            while (partWillBeSent < fullQueueSize + 1) {
                dataOutputStream.writeInt(partWillBeSent);
                int partServerDone = echoes.readInt();
                if ((partServerDone + 1) == partWillBeSent) {
                    byte[] bytes = queue.remove();  //each time send first byte[] element of the queue
                    dataOutputStream.write(bytes); //send here
                    partWillBeSent++;
                    System.out.println(partServerDone + "/" + fullQueueSize);
                    //dataOutputStream.writeInt(partWillBeSent);
                    continue;

                }
            }

            System.out.println(echoes.readInt() + "/" + fullQueueSize);



            //get success code
            while (true){
                try {
                    String response = echoes.readUTF();
                    if (response.equals(Config.SUCCESS_CODE)){
                        System.out.println("File ghi thành công ở server");
                        break;
                    }
                } catch (UTFDataFormatException e){

                }
            }

            fileInputStream.close();
            echoes.close();
            dataOutputStream.close();
            socket.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Mất kết nối tới server");
            return 1;
        }

    }
}