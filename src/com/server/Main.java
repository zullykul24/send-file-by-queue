package com.server;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) {
            try (ServerSocket serverSocket = new ServerSocket(5000)) {
                while (true) {
                    int n = new GetFile(serverSocket.accept()).get();
                    Thread.sleep(5000);
                    break;
                }

            } catch (IOException | InterruptedException e) {
                System.out.println("Server exception: " + e);
            }
    }
}
