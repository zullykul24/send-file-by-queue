package com.server;

import com.Config;

import java.io.*;
import java.net.Socket;

public class GetFile {
    private final Socket socket;


    public GetFile(Socket socket) {
        this.socket = socket;
    }


    public int get() {
        try {

            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());


            //receive fileName from client
            String fileName = dataInputStream.readUTF();
            File tempFile = new File("file_received", fileName);


            //receive queue size from client
            int queueSize = dataInputStream.readInt();
            System.out.println("queue size = " + queueSize);


            //receive remainder from client
            int remainder = dataInputStream.readInt();
            System.out.println("remainder = " + remainder);

            //count the number of times write into a new file
            int partDone = 0;
            byte[] elementBytes;

            System.out.println("Bắt đầu ghi file");

            while (partDone < queueSize){ //stop when write all elements of queue into file
                output.writeInt(partDone);
                int partWillBeWritten = dataInputStream.readInt();
                if(partWillBeWritten == (partDone + 1)) {
                    //if the last part
                    if((partDone + 1) == queueSize){
                        elementBytes = new byte[remainder];
                    } else {
                        elementBytes = new byte[Config.SIZE_EACH];
                    }


                    //store bytes in elementBytes[]
                    dataInputStream.readFully(elementBytes, 0, elementBytes.length);
                    try (FileOutputStream fileOutputStream = new FileOutputStream(tempFile, true);) {
                        ///write bytes into tempFile
                        fileOutputStream.write(elementBytes);
                        System.out.println(partDone + 1 + "/" + queueSize);
                        //increase when successfully wrote this part
                        partDone++;
                    }
                }
            }

            //when done all
            output.writeInt(partDone);

            System.out.println("Kết thúc ghi file");

            while (true) {
                //just a message to notice the client that server has written the file completely
                output.writeUTF(Config.SUCCESS_CODE);


                dataInputStream.close();
                output.close();
                socket.close();
                return 0;
            }

            //close

        }
        catch(Exception e) {
            e.printStackTrace();
            System.out.println("Lỗi nhận file");
            return 1;
        }
    }
}
